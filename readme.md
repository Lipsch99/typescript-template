# Typescript Server Template

## Contents

This template includes the following packages:

- Morgan: Logging
- Express: Webframework
- DotEnv: Managing environment variables
- Swagger: Documenting the HTTP API
- Nodemon: Restarting the server while developing
- Typescript: ..

## Get going

With `npm run build` the server is built into the `dist` directory.  
With `npm run start` the server ist started from the `dist` directory.  
With `npm run swagger` the swagger is rebuilt.  
With `npm run dev` the dev server is started.  
