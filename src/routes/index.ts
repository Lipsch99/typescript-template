import express from "express";
import HelloWorld from "../controllers/helloWorld";

const router = express.Router();

router.get("/", async (_req, res) => {
  const controller = new HelloWorld();
  const response = await controller.getMessage();
  return res.send(response);
});

export default router;