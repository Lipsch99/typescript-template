import { Get, Route } from "tsoa";

export interface MessageResponse {
  message: string;
}

@Route("/")
export default class HelloWorldController {
  @Get("/")
  public async getMessage(): Promise<MessageResponse> {
    return {
      message: "Hello World",
    };
  }
}