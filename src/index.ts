import express, { Application } from "express";
import morgan from "morgan";
import Router from "./routes";
const dotenv = require('dotenv');
import swaggerUi from "swagger-ui-express";
var cors = require('cors')



dotenv.config();

const PORT = process.env.PORT || 8000;

const app: Application = express();

app.use(cors({
  origin: 'http://localhost:3000',
  optionsSuccessStatus: 200
}))
app.use(express.json());
app.use(morgan("tiny"));
app.use(express.static("public"));

app.use(
  "/docs",
  swaggerUi.serve,
  swaggerUi.setup(undefined, {
    swaggerOptions: {
      url: "/swagger.json",
    },
  })
);

app.use(Router);

app.listen(PORT, () => {
  console.log("Server is running on port", PORT);
});